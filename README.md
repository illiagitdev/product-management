
****Web app for manging users(register, authorize), vendors and their products.****
-

---
*Technologies*
-

Java Core, PostgresQL, Spring (MVC, Data, Security, Spring Boot), HTML & CSS & Thymeleaf, Maven, Tomcat, Git

---
*Web application*
-

Page:

* Product(USER, ADMIN, ROOT)
    * Show all products - show list of products
    * Create products - create product form
    * Find product - find by name page
        - in product details available option to modify(update) product and DELETE button.
* Vendor(USER, ADMIN, ROOT)
    * Show all vendors - show list of vendors
    * Create vendor - create vendor form
    * Find vendor - find by name page
        - in vendor details available option to modify(update) vendor and DELETE button.
- User(ADMIN & ROOT)
    *Show all users - show list of users(ADMIN user can't see and manage users with role ROOT)
    * Create user - create user form
    * Find user - find by name page
        - in user details available option to modify(update) user and DELETE button.

*Entities and their description*
-
Product:
 
- has product name and price;
- and has provider(provider is mandatory when create product).
 
Provider:
 
- has name;
- has list of provided products.

User:

- has first and last name;
- has email(as username for authorisation);
- has password;
- has roles - determine permissions for user

Role:

- ROOT - allowed all operation, can change user roles
- ADMIN - allowed all CRUD operations
- USER - allowed READ-ONLY across site

---
*DataBase*
-

File: resources/db/postgresql/schema.sql contains database configuration and basic data for tables located in 
resources/db/postgresql/schema.sql. 
In file: resources/ERM.pdf shown Entity Relationship Diagram of database.

Vendors has One-to-Many relation with products.
Users has Many-to-Many relation with roles.

       
