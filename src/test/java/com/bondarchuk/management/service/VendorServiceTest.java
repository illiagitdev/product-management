package com.bondarchuk.management.service;

import com.bondarchuk.management.config.VendorConfig;
import com.bondarchuk.management.dao.VendorRepository;
import com.bondarchuk.management.entities.Vendor;
import com.bondarchuk.management.exception.VendorAlreadyExistsException;
import com.bondarchuk.management.exception.VendorNotExistsException;
import com.bondarchuk.management.helpers.VendorHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = VendorConfig.class)
public class VendorServiceTest {

    @MockBean
    private VendorRepository vendorRepository;

    @Autowired
    private VendorService vendorService;

    @Test
    public void testCreateVendorShouldReturnVendor() {
        //given
        final Vendor vendor = VendorHelper.createVendor(VendorHelper.FIRST_VENDOR_NAME);
        when(vendorRepository.findByName(vendor.getName())).thenReturn(Optional.empty());
        //when
        this.vendorService.create(vendor);
        //then
        assertThat(vendor.getName()).isEqualTo(VendorHelper.FIRST_VENDOR_NAME);
    }

    @Test(expected = VendorAlreadyExistsException.class)
    public void testCreateVendorShouldReturnException() {
        //given
        final Vendor vendor = VendorHelper.createVendor(VendorHelper.FIRST_VENDOR_NAME);
        when(vendorRepository.findByName(vendor.getName())).thenReturn(Optional.of(vendor));
        //when
        this.vendorService.create(vendor);
    }

    @Test
    public void testFindVendorShouldReturnVendor(){
        //given
        final Vendor vendor = VendorHelper.createVendor(VendorHelper.FIRST_VENDOR_NAME);
        when(vendorRepository.findById(VendorHelper.FIRST_VENDOR_ID)).thenReturn(Optional.of(vendor));
        //when
        this.vendorService.findVendor(VendorHelper.FIRST_VENDOR_ID.toString());
        //then
        assertThat(vendor.getName()).isEqualTo(VendorHelper.FIRST_VENDOR_NAME);
    }

    @Test(expected = VendorNotExistsException.class)
    public void testFindVendorShouldReturnException(){
        //given
        when(vendorRepository.findById(VendorHelper.FIRST_VENDOR_ID)).thenReturn(Optional.empty());
        //when
        this.vendorService.findVendor(VendorHelper.FIRST_VENDOR_ID.toString());
    }

    @Test
    public void testUpdateVendorShouldReturnVendorIfNoChanges() {
        //given
        final Vendor vendor = VendorHelper.createVendor(VendorHelper.FIRST_VENDOR_NAME);
        vendor.setId(VendorHelper.FIRST_VENDOR_ID);
        when(vendorRepository.findByName(VendorHelper.FIRST_VENDOR_NAME)).thenReturn(Optional.of(vendor));
        //when
        this.vendorService.update(vendor);
        //then
        assertThat(vendor.getName()).isEqualTo(VendorHelper.FIRST_VENDOR_NAME);
    }

    @Test
    public void testUpdateVendorShouldReturnVendorIfNewVendor() {
        //given
        final Vendor vendor = VendorHelper.createVendor(VendorHelper.FIRST_VENDOR_NAME);
        vendor.setId(VendorHelper.FIRST_VENDOR_ID);
        when(vendorRepository.findByName(VendorHelper.FIRST_VENDOR_NAME)).thenReturn(Optional.empty());
        //when
        this.vendorService.update(vendor);
        //then
        assertThat(vendor.getName()).isEqualTo(VendorHelper.FIRST_VENDOR_NAME);
    }

    @Test(expected = VendorAlreadyExistsException.class)
    public void testUpdateVendorShouldReturnException() {
        //given
        final Vendor vendor1 = VendorHelper.createVendor(VendorHelper.SECOND_VENDOR_NAME);
        vendor1.setId(VendorHelper.FIRST_VENDOR_ID);
        final Vendor vendor2 = VendorHelper.createVendor(VendorHelper.SECOND_VENDOR_NAME);
        vendor2.setId(VendorHelper.SECOND_VENDOR_ID);
        when(vendorRepository.findByName(VendorHelper.SECOND_VENDOR_NAME)).thenReturn(Optional.of(vendor2));
        //when
        this.vendorService.update(vendor1);
    }

    @Test
    public void testDeleteVendor() {
        //given when
        this.vendorService.delete(VendorHelper.SECOND_VENDOR_ID.toString());
    }

    @Test
    public void testGetVendorsListShouldReturnEmptyList () {
        //given
        when(vendorRepository.findAll()).thenReturn(List.of());
        //when
        final List<Vendor> vendors = this.vendorService.getList();
        //then
        assertThat(vendors.size()).isEqualTo(0);
    }

    @Test
    public void testGetVendorsListShouldReturnVendorsList () {
        //given
        when(vendorRepository.findAll()).thenReturn(VendorHelper.returnVendors());
        //when
        final List<Vendor> vendors = this.vendorService.getList();
        //then
        assertThat(vendors.size()).isEqualTo(3);
    }

    @Test
    public void testFindVendorByNameShouldReturnVendor() {
        //given
        final Vendor vendor = VendorHelper.createVendor(VendorHelper.FIRST_VENDOR_NAME);
        when(vendorRepository.findByName(VendorHelper.FIRST_VENDOR_NAME)).thenReturn(Optional.of(vendor));
        //when
        final Vendor vendorName = this.vendorService.findVendorName(VendorHelper.FIRST_VENDOR_NAME);
        //then
        assertThat(vendorName.getName()).isEqualTo(VendorHelper.FIRST_VENDOR_NAME);
    }

    @Test(expected = VendorNotExistsException.class)
    public void testFindVendorByNameShouldReturnException() {
        //given
        when(vendorRepository.findByName(VendorHelper.FIRST_VENDOR_NAME)).thenReturn(Optional.empty());
        //when
        this.vendorService.findVendorName(VendorHelper.FIRST_VENDOR_NAME);
    }

}
