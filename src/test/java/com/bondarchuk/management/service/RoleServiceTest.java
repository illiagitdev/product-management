package com.bondarchuk.management.service;

import com.bondarchuk.management.config.RoleConfig;
import com.bondarchuk.management.dao.RoleRepository;
import com.bondarchuk.management.entities.UserRole;
import com.bondarchuk.management.helpers.RoleHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = RoleConfig.class)
public class RoleServiceTest {

    @MockBean
    private RoleRepository roleRepository;

    @Autowired
    public RoleService roleService;

    @Test
    public void testFindUserRolesShouldReturnEmptyList() {
        //given
        when(roleRepository.findAll()).thenReturn(List.of());
        //when
        final List<UserRole> roles = this.roleService.findAll();
        //then
        assertThat(roles.size()).isEqualTo(0);
    }

    @Test
    public void testFindUserRolesShouldReturnRolesList() {
        //given
        when(roleRepository.findAll()).thenReturn(RoleHelper.prepareRoles());
        //when
        final List<UserRole> roles = this.roleService.findAll();
        //then
        assertThat(roles.size()).isEqualTo(3);
    }
}
