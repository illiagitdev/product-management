package com.bondarchuk.management.service;

import com.bondarchuk.management.config.ProductConfig;
import com.bondarchuk.management.config.VendorConfig;
import com.bondarchuk.management.dao.ProductRepository;
import com.bondarchuk.management.dao.VendorRepository;
import com.bondarchuk.management.entities.Product;
import com.bondarchuk.management.entities.Vendor;
import com.bondarchuk.management.exception.ProductAlreadyExistException;
import com.bondarchuk.management.exception.ProductNotExistsException;
import com.bondarchuk.management.exception.VendorNotExistsException;
import com.bondarchuk.management.helpers.ProductsHelper;
import com.bondarchuk.management.helpers.VendorHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ProductConfig.class, VendorConfig.class})
public class ProductServiceTest {

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private VendorRepository vendorRepository;

    @Autowired
    private ProductService productService;

    @Test
    public void testGetProductsListShouldReturnEmptyList() {
        //given
        when(productRepository.findAll()).thenReturn(List.of());
        //when
        final List<Product> products = this.productService.getList();
        //then
        assertThat(products.size()).isEqualTo(0);
    }

    @Test
    public void testGetProductsListShouldReturnList() {
        //given
        when(productRepository.findAll()).thenReturn(ProductsHelper.prepareProductsList());
        //when
        final List<Product> products = this.productService.getList();
        //then
        assertThat(products.size()).isEqualTo(3);
    }

    @Test
    public void testCreateProductShouldReturnProduct() {
        //given
        final Product product = ProductsHelper.createProduct(ProductsHelper.FIRST_PRODUCT_NAME,
                ProductsHelper.FIRST_PRODUCT_PRICE);
        final Vendor vendor = VendorHelper.createVendor(VendorHelper.FIRST_VENDOR_NAME);
        when(vendorRepository.findById(VendorHelper.FIRST_VENDOR_ID)).thenReturn(Optional.of(vendor));
        //when
        this.productService.create(product, VendorHelper.FIRST_VENDOR_ID.toString());
        //then
        assertThat(product.getName()).isEqualTo(ProductsHelper.FIRST_PRODUCT_NAME);
        assertThat(product.getPrice()).isEqualTo(ProductsHelper.FIRST_PRODUCT_PRICE);
    }

    @Test(expected = ProductAlreadyExistException.class)
    public void testCreateDuplicateProductShouldReturnException() {
        //given
        final Product product = ProductsHelper.createProduct(ProductsHelper.FIRST_PRODUCT_NAME,
                ProductsHelper.FIRST_PRODUCT_PRICE);
        when(productRepository.findByName(ProductsHelper.FIRST_PRODUCT_NAME)).thenReturn(Optional.of(product));
        //when
        this.productService.create(product, ProductsHelper.FIRST_PRODUCT_ID.toString());
    }

    @Test(expected = VendorNotExistsException.class)
    public void testCreateProductShouldReturnExceptionWithInexistentVendor() {
        //given
        final Product product = ProductsHelper.createProduct(ProductsHelper.FIRST_PRODUCT_NAME,
                ProductsHelper.FIRST_PRODUCT_PRICE);
        when(vendorRepository.findById(VendorHelper.FIRST_VENDOR_ID)).thenReturn(Optional.empty());
        //when
        this.productService.create(product, ProductsHelper.FIRST_PRODUCT_ID.toString());
        //then
    }

    @Test
    public void testFindProductShouldReturnProduct() {
        //given
        final Product product = ProductsHelper.createProduct(ProductsHelper.FIRST_PRODUCT_NAME,
                ProductsHelper.FIRST_PRODUCT_PRICE);
        when(productRepository.findById(ProductsHelper.FIRST_PRODUCT_ID)).thenReturn(Optional.of(product));
        //when
        this.productService.findProduct(ProductsHelper.FIRST_PRODUCT_ID.toString());
        //then
        assertThat(product.getName()).isEqualTo(ProductsHelper.FIRST_PRODUCT_NAME);
        assertThat(product.getPrice()).isEqualTo(ProductsHelper.FIRST_PRODUCT_PRICE);
    }

    @Test(expected = ProductNotExistsException.class)
    public void testFindProductShouldReturnException() {
        //given
        when(productRepository.findById(ProductsHelper.FIRST_PRODUCT_ID)).thenReturn(Optional.empty());
        //when
        this.productService.findProduct(ProductsHelper.FIRST_PRODUCT_ID.toString());
    }

    @Test
    public void testFindProductByNameShouldReturnProduct() {
        //given
        final Product product = ProductsHelper.createProduct(ProductsHelper.FIRST_PRODUCT_NAME,
                ProductsHelper.FIRST_PRODUCT_PRICE);
        when(productRepository.findByName(ProductsHelper.FIRST_PRODUCT_NAME)).thenReturn(Optional.of(product));
        //when
        this.productService.findProductName(ProductsHelper.FIRST_PRODUCT_NAME);
        //then
        assertThat(product.getName()).isEqualTo(ProductsHelper.FIRST_PRODUCT_NAME);
        assertThat(product.getPrice()).isEqualTo(ProductsHelper.FIRST_PRODUCT_PRICE);
    }

    @Test(expected = ProductNotExistsException.class)
    public void testFindProductByNameShouldReturnException() {
        //given
        when(productRepository.findByName(ProductsHelper.FIRST_PRODUCT_NAME)).thenReturn(Optional.empty());
        //when
        this.productService.findProductName(ProductsHelper.FIRST_PRODUCT_NAME);
    }

    @Test
    public void testUpdateProductShouldReturnProduct() {
        //given
        final Product product = ProductsHelper.persistedProduct(ProductsHelper.FIRST_PRODUCT_ID,
                ProductsHelper.FIRST_PRODUCT_NAME, ProductsHelper.FIRST_PRODUCT_PRICE);
        final Vendor vendor = VendorHelper.createVendor(VendorHelper.FIRST_VENDOR_NAME);
        when(vendorRepository.findById(VendorHelper.FIRST_VENDOR_ID)).thenReturn(Optional.of(vendor));
        when(productRepository.findById(ProductsHelper.FIRST_PRODUCT_ID)).thenReturn(Optional.of(product));
        //when
        product.setName(ProductsHelper.SECOND_PRODUCT_NAME);
        product.setPrice(ProductsHelper.SECOND_PRODUCT_PRICE);
        product.setVendor(null);
        this.productService.update(product, VendorHelper.FIRST_VENDOR_ID.toString());
        //then
        assertThat(product.getName()).isEqualTo(ProductsHelper.SECOND_PRODUCT_NAME);
        assertThat(product.getPrice()).isEqualTo(ProductsHelper.SECOND_PRODUCT_PRICE);
        assertThat(product.getVendor().getName()).isEqualTo(VendorHelper.FIRST_VENDOR_NAME);
    }

    @Test(expected = VendorNotExistsException.class)
    public void testUpdateProductShouldReturnException() {
        //given
        final Product product = ProductsHelper.persistedProduct(ProductsHelper.FIRST_PRODUCT_ID,
                ProductsHelper.FIRST_PRODUCT_NAME, ProductsHelper.FIRST_PRODUCT_PRICE);
        when(vendorRepository.findById(VendorHelper.FIRST_VENDOR_ID)).thenReturn(Optional.empty());
        when(productRepository.findById(ProductsHelper.FIRST_PRODUCT_ID)).thenReturn(Optional.of(product));
        //when
        product.setName(ProductsHelper.SECOND_PRODUCT_NAME);
        product.setPrice(ProductsHelper.SECOND_PRODUCT_PRICE);
        this.productService.update(product, VendorHelper.FIRST_VENDOR_ID.toString());
    }

    @Test
    public void testDeleteProductShouldReturnNull() {
        //given
        //when
        this.productService.delete(ProductsHelper.SECOND_PRODUCT_ID.toString());
    }
    @Test(expected = IllegalArgumentException.class)
    public void testDeleteProductWithInvalidId() {
        //given
        //when
        this.productService.delete("some-id");
    }

}
