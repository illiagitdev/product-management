package com.bondarchuk.management.service;

import com.bondarchuk.management.config.RoleConfig;
import com.bondarchuk.management.config.UserConfig;
import com.bondarchuk.management.dao.RoleRepository;
import com.bondarchuk.management.dao.UserRepository;
import com.bondarchuk.management.entities.Role;
import com.bondarchuk.management.entities.User;
import com.bondarchuk.management.entities.UserRole;
import com.bondarchuk.management.exception.RoleNotFoundException;
import com.bondarchuk.management.exception.UserAlreadyExistsException;
import com.bondarchuk.management.exception.UserNotExistsException;
import com.bondarchuk.management.helpers.RoleHelper;
import com.bondarchuk.management.helpers.UserHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {UserConfig.class, RoleConfig.class})
public class UserServiceTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private RoleRepository roleRepository;

    @Autowired
    private UserService userService;

    @Test
    public void testCreateUserShouldReturnUser(){
        //given
        final User user = UserHelper.firstUser();
        when(userRepository.findByEmail(UserHelper.FIRST_USER_EMAIL)).thenReturn(Optional.empty());
        when(roleRepository.findByRole(Role.ROLE_USER)).thenReturn(Optional.of(RoleHelper.setRole(RoleHelper.USER_ID, Role.ROLE_USER)));
        //when
        this.userService.create(user);
        //then
        assertThat(user.getPassword()).isNotEqualTo(UserHelper.FIRST_USER_PASSWORD);
        assertThat(user.getUserRoles().size()).isEqualTo(1);
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void testCreateUserWhenUserAlreadyExists(){
        //given
        final User user = UserHelper.firstUser();
        when(userRepository.findByEmail(UserHelper.FIRST_USER_EMAIL)).thenReturn(Optional.of(user));
        //when
        this.userService.create(user);
    }

    @Test(expected = RoleNotFoundException.class)
    public void testCreateUserWhenRoleNotFound() {
        //given
        final User user = UserHelper.firstUser();
        when(userRepository.findByEmail(UserHelper.FIRST_USER_EMAIL)).thenReturn(Optional.empty());
        when(roleRepository.findByRole(Role.ROLE_USER)).thenReturn(Optional.empty());
        //when
        this.userService.create(user);
    }

    @Test
    public void testGetUsersListShouldReturnUsers() {
        //given
        when(userRepository.findAll()).thenReturn(UserHelper.usersList());
        //when
        final List<User> users = this.userService.getList();
        //then
        assertThat(users.size()).isEqualTo(2);
    }

    @Test
    public void testGetUsersListShouldReturnEmptyList() {
        //given
        when(userRepository.findAll()).thenReturn(List.of());
        //when
        final List<User> users = this.userService.getList();
        //then
        assertThat(users.size()).isEqualTo(0);
    }

    @Test
    public void testGetUserByEmailShouldReturnUser() {
        //given
        final User user = UserHelper.firstUser();
        when(userRepository.findByEmail(UserHelper.FIRST_USER_EMAIL)).thenReturn(Optional.of(user));
        //when
        final User user1 = this.userService.getUser(UserHelper.FIRST_USER_EMAIL);
        //then
        assertThat(user1.getEmail()).isEqualTo(user.getEmail());
        assertThat(user1.getPassword()).isEqualTo(user.getPassword());
    }

    @Test(expected = UserNotExistsException.class)
    public void testGetUserByEmailShouldThrowException() {
        //given
        when(userRepository.findByEmail(UserHelper.FIRST_USER_EMAIL)).thenReturn(Optional.empty());
        //when
        this.userService.getUser(UserHelper.FIRST_USER_EMAIL);
    }

    @Test(expected = UserNotExistsException.class)
    public void testGetUserByEmailShouldWithIncorrectEmail() {
        //given
        final User user = UserHelper.firstUser();
        when(userRepository.findByEmail(UserHelper.FIRST_USER_EMAIL)).thenReturn(Optional.of(user));
        //when
        this.userService.getUser(UserHelper.INCORRECT_EMAIL);
    }

    @Test
    public void testFindUserByIdShouldReturnUser(){
        //given
        final User user = UserHelper.firstUser();
        when(userRepository.findById(UserHelper.FIRST_USER_ID)).thenReturn(Optional.of(user));
        //when
        final User user1 = this.userService.findUser(UserHelper.FIRST_USER_ID.toString());
        //then
        assertThat(user1.getEmail()).isEqualTo(user.getEmail());
        assertThat(user1.getPassword()).isEqualTo(user.getPassword());
    }

    @Test(expected = UserNotExistsException.class)
    public void testFindUserByIdShouldThrowException(){
        //given
        final User user = UserHelper.firstUser();
        when(userRepository.findById(UserHelper.FIRST_USER_ID)).thenReturn(Optional.of(user));
        //when
        this.userService.findUser(UserHelper.SECOND_USER_ID.toString());
    }

    @Test
    public void testDeleteUser() {
        //given then
        this.userService.delete(UserHelper.FIRST_USER_ID.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteUserShouldThrowException() {
        //given then
        this.userService.delete("some-id");
    }

    @Test
    public void testUpdateUser() {
        //given
        final User user = UserHelper.firstUser();
        final UserRole userRole = RoleHelper.setRole(RoleHelper.USER_ID, Role.ROLE_USER);
        user.setUserRoles(List.of(userRole));
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        //when
        user.setFirstName(UserHelper.SECOND_USER_NAME);
        user.setLastName(UserHelper.SECOND_USER_LAST_NAME);
        user.setPassword(UserHelper.SECOND_USER_PASSWORD);
        this.userService.update(user);
        //then
        assertThat(user.getUserRoles().size()).isEqualTo(1);
        assertThat(user.getPassword()).isNotEqualTo(UserHelper.SECOND_USER_PASSWORD);
        assertThat(user.getEmail()).isEqualTo(UserHelper.FIRST_USER_EMAIL);
    }

    @Test
    public void testUpdateRolesShouldReturnUser() {
        //given
        final User user = UserHelper.createUser(UserHelper.FIRST_USER_NAME, UserHelper.FIRST_USER_LAST_NAME,
                UserHelper.FIRST_USER_EMAIL, UserHelper.FIRST_USER_PASSWORD);
        user.setId(UserHelper.FIRST_USER_ID);
        final UserRole userRole = RoleHelper.setRole(RoleHelper.ADMIN_ID, Role.ROLE_ADMIN);
        final UserRole newRole = RoleHelper.setRole(RoleHelper.USER_ID, Role.ROLE_USER);
        user.setUserRoles(List.of(userRole));
        List<UserRole> updateRoles = List.of(userRole, newRole);
        when(userRepository.findById(UserHelper.FIRST_USER_ID)).thenReturn(Optional.of(user));
        //when
        this.userService.updateRoles(UserHelper.FIRST_USER_ID.toString(), updateRoles);
        this.userService.findUser(UserHelper.FIRST_USER_ID.toString());
        //then
        assertThat(user.getUserRoles().size()).isEqualTo(2);
    }

    @Test(expected = UserNotExistsException.class)
    public void testUpdateUserRolesShouldReturnException() {
        //given
        final UserRole userRole = RoleHelper.setRole(RoleHelper.ADMIN_ID, Role.ROLE_ADMIN);
        when(userRepository.findById(UserHelper.FIRST_USER_ID)).thenReturn(Optional.empty());
        //when
        this.userService.updateRoles(UserHelper.FIRST_USER_ID.toString(), List.of(userRole));
    }

}
