package com.bondarchuk.management.helpers;

import com.bondarchuk.management.entities.Product;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class ProductsHelper {
    public static final String FIRST_PRODUCT_NAME = "Moon glasses";
    public static final BigDecimal FIRST_PRODUCT_PRICE = new BigDecimal(50.5);
    public static final UUID FIRST_PRODUCT_ID = UUID.fromString("462d3e2b-9f7d-42e8-87ee-860685490a5a");
    public static final String SECOND_PRODUCT_NAME = "Moon boots";
    public static final BigDecimal SECOND_PRODUCT_PRICE = new BigDecimal(100.5);
    public static final UUID SECOND_PRODUCT_ID = UUID.fromString("cc35fc02-9642-43f2-9e21-0390fce6a086");
    public static final String THIRD_PRODUCT_NAME = "Moon trip";
    public static final BigDecimal THIRD_PRODUCT_PRICE = new BigDecimal(200.5);
    public static final UUID THIRD_PRODUCT_ID = UUID.fromString("1ab1415e-4d4a-4141-9f08-3bbf9c5f9a0c");

    public static List<Product> prepareProductsList() {
        return List.of(createProduct(FIRST_PRODUCT_NAME, FIRST_PRODUCT_PRICE),
                createProduct(SECOND_PRODUCT_NAME, SECOND_PRODUCT_PRICE),
                createProduct(THIRD_PRODUCT_NAME, THIRD_PRODUCT_PRICE));
    }

    public static Product createProduct(String name, BigDecimal price) {
        Product product = new Product();
        product.setName(name);
        product.setPrice(price);
        return product;
    }

    public static Product persistedProduct(UUID id, String name, BigDecimal price) {
        final Product product = createProduct(name, price);
        product.setId(id);
        return product;
    }
}
