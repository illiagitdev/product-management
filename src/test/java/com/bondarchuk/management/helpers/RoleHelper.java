package com.bondarchuk.management.helpers;

import com.bondarchuk.management.entities.Role;
import com.bondarchuk.management.entities.UserRole;

import java.util.List;
import java.util.UUID;

public class RoleHelper {
    public static final UUID USER_ID = UUID.fromString("258e5445-8d0b-4415-b89a-66b3a8093f62");
    public static final UUID ADMIN_ID = UUID.fromString("77cb495f-9702-4f48-befb-1b60db3343f0");
    public static final UUID ROOT_ID = UUID.fromString("52240ffc-9bf9-499d-a21b-8ed7cf3155a1");

    public static List<UserRole> prepareRoles() {
        return List.of(setRole(ROOT_ID, Role.ROLE_ROOT), setRole(ADMIN_ID, Role.ROLE_ADMIN),
                setRole(USER_ID, Role.ROLE_USER));
    }

    public static UserRole setRole(UUID id, Role role) {
        UserRole userRole = new UserRole();
        userRole.setId(id);
        userRole.setRole(role);
        return userRole;
    }
}
