package com.bondarchuk.management.helpers;

import com.bondarchuk.management.entities.Vendor;

import java.util.List;
import java.util.UUID;

public class VendorHelper {
    public static final String FIRST_VENDOR_NAME = "Moon";
    public static final UUID FIRST_VENDOR_ID = UUID.fromString("9f69af7e-c2f2-4bca-a3c4-6a23d0897b8f");
    public static final String SECOND_VENDOR_NAME = "Trion";
    public static final UUID SECOND_VENDOR_ID = UUID.fromString("72084d13-85df-4596-9bf3-bfd6ed1983bc");
    public static final String THIRD_VENDOR_NAME = "Star";
    public static final UUID THIRD_VENDOR_ID = UUID.fromString("34b77030-da6b-4e14-a577-9f70f8855316");

    public static Vendor createVendor(String name) {
        Vendor vendor = new Vendor();
        vendor.setName(name);
        return vendor;
    }

    public static List<Vendor> returnVendors() {
        return List.of(createVendor(FIRST_VENDOR_NAME), createVendor(SECOND_VENDOR_NAME),
                createVendor(THIRD_VENDOR_NAME));
    }
}
