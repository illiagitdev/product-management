package com.bondarchuk.management.helpers;

import com.bondarchuk.management.entities.User;

import java.util.List;
import java.util.UUID;

public class UserHelper {
    public static final String FIRST_USER_NAME = "Barack";
    public static final String FIRST_USER_LAST_NAME = "Obama";
    public static final UUID FIRST_USER_ID = UUID.fromString("842bcbf1-40d6-4b60-ae68-c542b9c6c1a9");
    public static final String FIRST_USER_EMAIL = "barack@mail.com";
    public static final String FIRST_USER_PASSWORD = "123";
    public static final String SECOND_USER_NAME = "Bill";
    public static final String SECOND_USER_LAST_NAME = "Klinton";
    public static final UUID SECOND_USER_ID = UUID.fromString("d6a21357-dc2a-416a-9c2e-fd511cfa5bb5");
    public static final String SECOND_USER_EMAIL = "bill@mail.com";
    public static final String SECOND_USER_PASSWORD = "1234";
    public static final String INCORRECT_EMAIL = "some@mail.com";

    public static User createUser(String name, String lastName,String email, String password){
        User user = new User();
        user.setFirstName(name);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(password);
        return user;
    }

    public static List<User> usersList() {
        return List.of(createUser(FIRST_USER_NAME, FIRST_USER_LAST_NAME, FIRST_USER_EMAIL, FIRST_USER_PASSWORD),
                createUser(SECOND_USER_NAME, SECOND_USER_LAST_NAME, SECOND_USER_EMAIL, SECOND_USER_PASSWORD));
    }

    public static User firstUser(){
        final User user = createUser(FIRST_USER_NAME, FIRST_USER_LAST_NAME, FIRST_USER_EMAIL, FIRST_USER_PASSWORD);
        user.setId(FIRST_USER_ID);
        return user;
    }
}
