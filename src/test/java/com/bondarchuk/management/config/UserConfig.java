package com.bondarchuk.management.config;

import com.bondarchuk.management.dao.RoleRepository;
import com.bondarchuk.management.dao.UserRepository;
import com.bondarchuk.management.service.UserService;
import com.bondarchuk.management.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@TestConfiguration
@ComponentScan(basePackages = {"com.bondarchuk.management.dao"})
public class UserConfig {

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public RoleRepository roleRepository;

    @Bean
    public UserService userService() {
        return new UserServiceImpl(userRepository, roleRepository);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder(12);
    }

}
