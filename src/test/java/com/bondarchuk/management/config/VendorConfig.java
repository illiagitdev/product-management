package com.bondarchuk.management.config;

import com.bondarchuk.management.dao.VendorRepository;
import com.bondarchuk.management.service.VendorService;
import com.bondarchuk.management.service.VendorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@TestConfiguration
@ComponentScan(basePackages = {"com.bondarchuk.management.dao"})
public class VendorConfig {

    @Autowired
    public VendorRepository vendorRepository;

    @Bean
    public VendorService vendorService() {
        return new VendorServiceImpl(vendorRepository);
    }

}
