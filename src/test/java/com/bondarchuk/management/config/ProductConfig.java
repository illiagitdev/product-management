package com.bondarchuk.management.config;

import com.bondarchuk.management.dao.ProductRepository;
import com.bondarchuk.management.dao.VendorRepository;
import com.bondarchuk.management.service.ProductService;
import com.bondarchuk.management.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@TestConfiguration
@ComponentScan(basePackages = {"com.bondarchuk.management.dao"})
public class ProductConfig {

    @Autowired
    public ProductRepository productRepository;

    @Autowired
    public VendorRepository vendorRepository;

    @Bean
    public ProductService productService() {
        return new ProductServiceImpl(productRepository, vendorRepository);
    }
}
