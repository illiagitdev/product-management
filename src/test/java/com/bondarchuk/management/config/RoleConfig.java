package com.bondarchuk.management.config;

import com.bondarchuk.management.dao.RoleRepository;
import com.bondarchuk.management.service.RoleService;
import com.bondarchuk.management.service.UserRoleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@TestConfiguration
@ComponentScan(basePackages = {"com.bondarchuk.management.dao"})
public class RoleConfig {

    @Autowired
    public RoleRepository roleRepository;

    @Bean
    public RoleService roleService() {
        return new UserRoleServiceImpl(roleRepository);
    }

}
