CREATE TABLE vendors (
  id UUID,
  name VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE products (
  id UUID,
  name VARCHAR(100) NOT NULL ,
  price NUMERIC(14, 4),
  vendor_id UUID,
  PRIMARY KEY (id),
  CHECK (price > 0),
  FOREIGN KEY (vendor_id)
    REFERENCES vendors (id),
  UNIQUE (name)
);

CREATE TABLE roles (
  id UUID,
  role VARCHAR(15),
  PRIMARY KEY (id),
  UNIQUE (role)
);

CREATE TABLE users (
  id UUID,
  email VARCHAR(50),
  password VARCHAR(60),
  first_name VARCHAR(50) NOT NULL ,
  last_name VARCHAR(50),
  PRIMARY KEY (id),
  UNIQUE (email)
);

CREATE TABLE users_roles (
    role_id UUID,
    user_id UUID,
    PRIMARY KEY (role_id, user_id),
    FOREIGN KEY (role_id)
        REFERENCES roles (id),
    FOREIGN KEY (user_id)
        REFERENCES users (id)
);
