-- fill up for UserRole table
INSERT INTO roles(id, role) VALUES ('52240ffc-9bf9-499d-a21b-8ed7cf3155a1', 'ROLE_ROOT');
INSERT INTO roles(id, role) VALUES ('77cb495f-9702-4f48-befb-1b60db3343f0', 'ROLE_ADMIN');
INSERT INTO roles(id, role) VALUES ('258e5445-8d0b-4415-b89a-66b3a8093f62', 'ROLE_USER');
INSERT INTO roles(id, role) VALUES ('f9d41d49-f1d8-43a6-9520-305c1d6a74f5', 'ROLE_GUEST');

-- fill up for Users table
-- password:password
INSERT INTO users(id, email, password, first_name, last_name) VALUES ('842bcbf1-40d6-4b60-ae68-c542b9c6c1a9', 'userRoot@user.com', '$2y$12$zZ8yEu54iCfSjIH47Y2PFOHnv5hJKTAJaVzGkwKmBsQcR0Mc4qKTW', 'First Name', 'Last Name');
INSERT INTO users(id, email, password, first_name, last_name) VALUES ('d6a21357-dc2a-416a-9c2e-fd511cfa5bb5', 'userAdmin@user.com', '$2y$12$zZ8yEu54iCfSjIH47Y2PFOHnv5hJKTAJaVzGkwKmBsQcR0Mc4qKTW', 'First Name', 'Last Name');
INSERT INTO users(id, email, password, first_name, last_name) VALUES ('75fdf9f7-d1c8-41a6-b358-28210c31cf9b', 'userUser@user.com', '$2y$12$zZ8yEu54iCfSjIH47Y2PFOHnv5hJKTAJaVzGkwKmBsQcR0Mc4qKTW', 'First Name', 'Last Name');
INSERT INTO users(id, email, password, first_name, last_name) VALUES ('1160b81a-56f3-4ae6-a366-fd91c66041c9', 'userGuest@user.com', '$2y$12$zZ8yEu54iCfSjIH47Y2PFOHnv5hJKTAJaVzGkwKmBsQcR0Mc4qKTW', 'First Name', 'Last Name');

-- fill up for Users-Roles table
INSERT INTO users_roles(role_id, user_id) VALUES ('52240ffc-9bf9-499d-a21b-8ed7cf3155a1', '842bcbf1-40d6-4b60-ae68-c542b9c6c1a9');
INSERT INTO users_roles(role_id, user_id) VALUES ('77cb495f-9702-4f48-befb-1b60db3343f0', '842bcbf1-40d6-4b60-ae68-c542b9c6c1a9');
INSERT INTO users_roles(role_id, user_id) VALUES ('77cb495f-9702-4f48-befb-1b60db3343f0', 'd6a21357-dc2a-416a-9c2e-fd511cfa5bb5');
INSERT INTO users_roles(role_id, user_id) VALUES ('258e5445-8d0b-4415-b89a-66b3a8093f62', 'd6a21357-dc2a-416a-9c2e-fd511cfa5bb5');
INSERT INTO users_roles(role_id, user_id) VALUES ('258e5445-8d0b-4415-b89a-66b3a8093f62', '75fdf9f7-d1c8-41a6-b358-28210c31cf9b');
INSERT INTO users_roles(role_id, user_id) VALUES ('f9d41d49-f1d8-43a6-9520-305c1d6a74f5', '1160b81a-56f3-4ae6-a366-fd91c66041c9');

-- fill up for Vendors table
INSERT INTO vendors(id, name) VALUES ('9f69af7e-c2f2-4bca-a3c4-6a23d0897b8f', 'Moon');
INSERT INTO vendors(id, name) VALUES ('72084d13-85df-4596-9bf3-bfd6ed1983bc', 'Trion');
INSERT INTO vendors(id, name) VALUES ('34b77030-da6b-4e14-a577-9f70f8855316', 'Star');

-- fill up for Products table
INSERT INTO products(id, name, price, vendor_id) VALUES ('462d3e2b-9f7d-42e8-87ee-860685490a5a', 'Moon glasses', '12.5000', '9f69af7e-c2f2-4bca-a3c4-6a23d0897b8f');
INSERT INTO products(id, name, price, vendor_id) VALUES ('cc35fc02-9642-43f2-9e21-0390fce6a086', 'Moon boots', '22.0000', '9f69af7e-c2f2-4bca-a3c4-6a23d0897b8f');
INSERT INTO products(id, name, price, vendor_id) VALUES ('1ab1415e-4d4a-4141-9f08-3bbf9c5f9a0c', 'Moon trip', '17000.0000', '9f69af7e-c2f2-4bca-a3c4-6a23d0897b8f');
INSERT INTO products(id, name, price, vendor_id) VALUES ('a0f45663-ccb7-4e0c-bd86-12e65efbc1e9', 'Supercar', '1250000.0000', '72084d13-85df-4596-9bf3-bfd6ed1983bc');
INSERT INTO products(id, name, price, vendor_id) VALUES ('1c47528b-e84c-4f36-bdaa-d3e6f4f45fb0', 'Space ship', '40500000.0000', '72084d13-85df-4596-9bf3-bfd6ed1983bc');
INSERT INTO products(id, name, price, vendor_id) VALUES ('d4b9e9c0-1012-485a-95af-337de300eb03', 'Telescope', '9058000.0000', '72084d13-85df-4596-9bf3-bfd6ed1983bc');
INSERT INTO products(id, name, price, vendor_id) VALUES ('70e9c48e-5ffd-490d-8e6d-8059fc43f2c8', 'Star t-shirt', '22.0000', '34b77030-da6b-4e14-a577-9f70f8855316');
INSERT INTO products(id, name, price, vendor_id) VALUES ('ee6a69c8-aad6-4e71-aa78-4c4e2a995617', 'Star cup', '34.0000', '34b77030-da6b-4e14-a577-9f70f8855316');
INSERT INTO products(id, name, price, vendor_id) VALUES ('77ac25da-7ff8-42e0-88d9-35cca0611807', 'Solar book ', '150.0000', '34b77030-da6b-4e14-a577-9f70f8855316');
INSERT INTO products(id, name, price, vendor_id) VALUES ('40719c52-a04f-4463-914e-ab6471d94c46', 'Neutron star model', '7500.0000', '34b77030-da6b-4e14-a577-9f70f8855316');
