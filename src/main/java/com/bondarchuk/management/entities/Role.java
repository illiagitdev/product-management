package com.bondarchuk.management.entities;

import java.util.Arrays;
import java.util.Optional;

public enum Role {
    ROLE_USER("ROLE_USER"),
    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_GUEST("ROLE_GUEST"),
    ROLE_ROOT("ROLE_ROOT");

    private String role;

    Role(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public static Optional<Role> getRole(String status) {
        return Arrays.stream(Role.values())
                .filter(enumValue -> enumValue.getRole().equals(status))
                .findAny();
    }
}
