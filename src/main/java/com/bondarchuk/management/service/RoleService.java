package com.bondarchuk.management.service;

import com.bondarchuk.management.entities.UserRole;

import java.util.List;

public interface RoleService {
    List<UserRole> findAll();
}
