package com.bondarchuk.management.service;

import com.bondarchuk.management.entities.Vendor;

import java.util.List;

public interface VendorService {

    void create(Vendor vendor);
    Vendor findVendor(String id);
    void update(Vendor vendor);
    void delete(String id);
    List<Vendor> getList();
    Vendor findVendorName(String name);

}
