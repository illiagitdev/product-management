package com.bondarchuk.management.service;


import com.bondarchuk.management.dao.ProductRepository;
import com.bondarchuk.management.dao.VendorRepository;
import com.bondarchuk.management.entities.Product;
import com.bondarchuk.management.entities.Vendor;
import com.bondarchuk.management.exception.ProductAlreadyExistException;
import com.bondarchuk.management.exception.ProductNotExistsException;
import com.bondarchuk.management.exception.VendorNotExistsException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService{
    private static final Logger LOG = LogManager.getLogger(ProductServiceImpl.class);
    private ProductRepository productRepository;
    private VendorRepository vendorRepository;

    public ProductServiceImpl(ProductRepository productRepository, VendorRepository vendorRepository) {
        this.productRepository = productRepository;
        this.vendorRepository = vendorRepository;
    }

    @Override
    public List<Product> getList() {
        return productRepository.findAll();
    }

    @Override
    public void create(Product product, String vendorId) {
        if (productExists(product.getName())) {
            LOG.error(String.format("Product with name '%s' already exists.", product.getName()));
            throw new ProductAlreadyExistException(
                    String.format("Product with name '%s' already exists.", product.getName()));
        }
        Vendor vendor = vendorRepository.findById(UUID.fromString(vendorId)).orElseThrow(() ->
                new VendorNotExistsException("Vendor not found/"));
        product.setVendor(vendor);
        productRepository.save(product);
    }

    @Override
    public Product findProduct(String id) {
        UUID uuid = UUID.fromString(id);
        LOG.debug(String.format("findProduct: uuid = %s", uuid));
        return productRepository.findById(uuid).orElseThrow(() ->
                new ProductNotExistsException("Product not found."));
    }

    @Override
    public Product findProductName(String name) {
        LOG.debug(String.format("findProductName: name = %s", name));
        return productRepository.findByName(name).orElseThrow(() ->
                new ProductNotExistsException(String.format("Product with name '%s' not found!", name)));
    }

    @Override
    public void update(Product product, String vendorId) {
        Vendor vendor = vendorRepository.findById(UUID.fromString(vendorId)).orElseThrow(() ->
                new VendorNotExistsException("Vendor not found/"));
        product.setVendor(vendor);
        productRepository.save(product);
    }

    @Override
    public void delete(String id) {
        productRepository.deleteById(UUID.fromString(id));
    }

    private boolean productExists(String name) {
        return productRepository.findByName(name).isPresent();
    }
}
