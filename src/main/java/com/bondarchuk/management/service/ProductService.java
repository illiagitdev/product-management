package com.bondarchuk.management.service;

import com.bondarchuk.management.entities.Product;

import java.util.List;

public interface ProductService {

    List<Product> getList();
    void create(Product product, String vendorId);
    Product findProduct(String id);
    Product findProductName(String name);
    void update(Product product, String vendorId);
    void delete(String id);

}
