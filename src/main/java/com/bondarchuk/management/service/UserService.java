package com.bondarchuk.management.service;

import com.bondarchuk.management.entities.User;
import com.bondarchuk.management.entities.UserRole;

import java.util.List;

public interface UserService {

    void create(User user);
    List<User> getList();
    User getUser(String email);
    User findUser(String id);
    void delete(String id);
    void update(User user);
    void updateRoles(String id, List<UserRole> userRoles);

}
