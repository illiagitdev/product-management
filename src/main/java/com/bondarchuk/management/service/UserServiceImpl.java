package com.bondarchuk.management.service;

import com.bondarchuk.management.dao.RoleRepository;
import com.bondarchuk.management.dao.UserRepository;
import com.bondarchuk.management.entities.Role;
import com.bondarchuk.management.entities.User;
import com.bondarchuk.management.entities.UserRole;
import com.bondarchuk.management.exception.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger LOG = LogManager.getLogger(UserServiceImpl.class);
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder encoder;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Autowired
    public void setEncoder(BCryptPasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @Override
    public void create(User user) {
        userExists(user);
        List<UserRole> userRoles = Collections.singletonList(roleRepository.findByRole(Role.ROLE_USER).
                orElseThrow(() -> new RoleNotFoundException(String.format("Role not found (%s).", Role.ROLE_USER))));
        user.setUserRoles(userRoles);
        user.setPassword(encoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public List<User> getList() {
        return userRepository.findAll();
    }

    @Override
    public User getUser(String email) {
        return userRepository.findByEmail(email).orElseThrow(() ->
                new UserNotExistsException(String.format("User with email '%s' not found.", email)));
    }

    @Override
    public User findUser(String id) {
        UUID uuid = UUID.fromString(id);
        LOG.debug(String.format("findUser: id = %s", id));
        return userRepository.findById(uuid).orElseThrow(() -> new UserNotExistsException("User not found."));
    }

    @Override
    public void delete(String id) {
        LOG.debug(String.format("delete: id=%s", id));
        userRepository.deleteById(UUID.fromString(id));
    }

    @Override
    public void update(User user) {
        List<UserRole> roles = userRepository.findById(user.getId()).get().getUserRoles();
        user.setUserRoles(roles);
        user.setPassword(encoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public void updateRoles(String id, List<UserRole> userRoles) {
        User user = userRepository.findById(UUID.fromString(id)).orElseThrow(() ->
                new UserNotExistsException("User not found."));
        user.setUserRoles(userRoles);
        userRepository.save(user);
    }

    private void userExists(User user) {
        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            LOG.error(String.format("create: email = %s", user.getEmail()));
            throw new UserAlreadyExistsException(
                    String.format("User with such email(login) = '%s' already exists.", user.getEmail()));
        }
    }
}
