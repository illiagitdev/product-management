package com.bondarchuk.management.service;

import com.bondarchuk.management.dao.RoleRepository;
import com.bondarchuk.management.entities.UserRole;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRoleServiceImpl implements RoleService {
    private RoleRepository roleRepository;

    public UserRoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<UserRole> findAll() {
        return roleRepository.findAll();
    }
}
