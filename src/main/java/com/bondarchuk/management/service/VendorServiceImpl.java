package com.bondarchuk.management.service;

import com.bondarchuk.management.dao.VendorRepository;
import com.bondarchuk.management.entities.Vendor;
import com.bondarchuk.management.exception.VendorAlreadyExistsException;
import com.bondarchuk.management.exception.VendorNotExistsException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class VendorServiceImpl implements VendorService {
    private static final Logger LOG = LogManager.getLogger(VendorServiceImpl.class);
    private VendorRepository vendorRepository;

    public VendorServiceImpl(VendorRepository vendorRepository) {
        this.vendorRepository = vendorRepository;
    }

    @Override
    public void create(Vendor vendor) {
        if (vendorExists(vendor.getName())) {
            LOG.error(String.format("Vendor with name '%s' already exists.", vendor.getName()));
            throw new VendorAlreadyExistsException(
                    String.format("Vendor with name '%s' already exists.", vendor.getName()));
        }
        vendorRepository.save(vendor);
    }

    @Override
    public Vendor findVendor(String id) {
        UUID uuid = UUID.fromString(id);
        LOG.debug(String.format("findVendor: %s", uuid));
        return vendorRepository.findById(uuid).orElseThrow(() ->
                new VendorNotExistsException("Vendor not found."));
    }

    @Override
    public void update(Vendor vendor) {
        if (vendorDuplicate(vendor)) {
            LOG.error(String.format("update: %s", vendor.getName()));
            throw new VendorAlreadyExistsException(
                    String.format("Vendor with name '%s' already exists.", vendor.getName()));

        }
        vendorRepository.save(vendor);
    }

    @Override
    public void delete(String id) {
        vendorRepository.deleteById(UUID.fromString(id));
    }

    @Override
    public List<Vendor> getList() {
        return vendorRepository.findAll();
    }

    @Override
    public Vendor findVendorName(String name) {
        LOG.debug(String.format("findVendorName: name = %s", name));
        return vendorRepository.findByName(name).orElseThrow(() ->
                new VendorNotExistsException(String.format("Vendor with name '%s' not found!", name)));

    }

    private boolean vendorDuplicate(Vendor vendor) {
        final Optional<Vendor> byName = vendorRepository.findByName(vendor.getName());
        return byName.map(value -> !value.getId().equals(vendor.getId())).orElse(false);
    }

    private boolean vendorExists(String name) {
        return vendorRepository.findByName(name).isPresent();
    }
}
