package com.bondarchuk.management.dao;

import com.bondarchuk.management.entities.Role;
import com.bondarchuk.management.entities.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RoleRepository extends JpaRepository<UserRole, UUID> {

    Optional<UserRole> findByRole(Role role);
}
