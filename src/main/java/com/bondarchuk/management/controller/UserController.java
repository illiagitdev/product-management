package com.bondarchuk.management.controller;

import com.bondarchuk.management.entities.Role;
import com.bondarchuk.management.entities.User;
import com.bondarchuk.management.entities.UserRole;
import com.bondarchuk.management.exception.UserAlreadyExistsException;
import com.bondarchuk.management.exception.UserNotExistsException;
import com.bondarchuk.management.service.RoleService;
import com.bondarchuk.management.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/user")
public class UserController {
    private UserService users;
    private RoleService roles;

    @Autowired
    public void setUsers(UserService users) {
        this.users = users;
    }

    @Autowired
    public void setRoles(RoleService roles) {
        this.roles = roles;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')")
    @GetMapping(path = "/addUser")
    public String getAddUserView() {
        return "user-create";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')")
    @PostMapping(path = "/addUser")
    public String addUser(@Valid @ModelAttribute("user") User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "user-create";
        }

        try {
            users.create(user);
        } catch (UserAlreadyExistsException ex) {
            model.addAttribute("error", ex.getMessage());
            return "error";
        }

        model.addAttribute("user", user);
        return "user-details";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')")
    @GetMapping(path = "/userList")
    public String showUserList(Model model) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (userDetails.getAuthorities().stream().anyMatch(role -> role.getAuthority().equals(Role.ROLE_ROOT.getRole()))) {
            model.addAttribute("users", users.getList());
        } else {
            model.addAttribute("users", users.getList().stream().filter(
                    user -> user.getUserRoles().stream().noneMatch(userRole -> userRole.getRole().equals(Role.ROLE_ROOT))
            ).collect(Collectors.toList()));
        }
        return "user-list";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')")
    @GetMapping(path = "/findUser")
    public String findUserView() {
        return "user-find";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')")
    @GetMapping(path = "/find")
    public String getUser(@RequestParam("email") String email, Model model) {
        User user = null;

        try {
            user = users.getUser(email);
        } catch (UserNotExistsException ex) {
            model.addAttribute("error", ex.getMessage());
            return "user-find";
        }

        model.addAttribute("user", user);
        return "user-details";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')")
    @GetMapping(path = "/{id}/details")
    public String showUserDetails(@PathVariable("id") String id, Model model) {
        model.addAttribute("user", users.findUser(id));
        return "user-details";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')")
    @GetMapping(path = "/{id}/delete")
    public String delete(@PathVariable("id") String id) {
        users.delete(id);
        return "redirect:/index";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT')")
    @GetMapping(path = "/{id}/updatePage")
    public String getUpdateUserView(@PathVariable("id") String id, Model model) {
        model.addAttribute("user", users.findUser(id));
        return "user-update";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT') or hasRole('ROLE_USER')")
    @GetMapping(path = "/updatePage")
    public String getUpdateUserSettingsView(@RequestParam("username") String username, Model model) {
        model.addAttribute("user", users.getUser(username));
        return "user-update";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT') or hasRole('ROLE_USER')")
    @PostMapping(path = "/{id}/update")
    public String updateUser(@Valid @ModelAttribute("user") User user, @PathVariable("id") String id,
                           BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("user", users.findUser(id));
            return "user-update";
        }

        try {
            users.update(user);
        } catch (UserAlreadyExistsException ex) {
            model.addAttribute("error", ex.getMessage());
            return "redirect:/error";
        }

        model.addAttribute("user", user);
        return "user-details";

    }

    @GetMapping("/registration")
    public String showRegistrationForm() {
        return "registration";
    }

    @PostMapping(path = "/registration")
    public String userRegistration(@Valid @ModelAttribute("user") User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "registration";
        }

        try {
            users.create(user);
        } catch (UserAlreadyExistsException ex) {
            model.addAttribute("message", "User with this username already exists.");
            return "registration";
        }

        return "redirect:/login";
    }

    @GetMapping(path = "/{id}/updateRole")
    @PreAuthorize("hasRole('ROLE_ROOT')")
    public String updateUserRolesView(@PathVariable("id") String id, Model model) {
        model.addAttribute("user", users.findUser(id));
        List<UserRole> roles = this.roles.findAll();
        model.addAttribute("roles", roles);
        return "user-update-role";
    }

    @PostMapping(path = "/{id}/updateRole")
    @PreAuthorize("hasRole('ROLE_ROOT')")
    public String updateUserRoles(@PathVariable("id") String id, @ModelAttribute("user") User user,
                                  BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("error", "Error updating roles, contact support");
            return "redirect:/error";
        }
        users.updateRoles(id, user.getUserRoles());
        return "redirect:/index";
    }

    @ModelAttribute
    public List<UserRole> listOfRoles() {
        return new ArrayList<>();
    }

    @ModelAttribute
    public User defaultUser() {
        return new User();
    }
}
