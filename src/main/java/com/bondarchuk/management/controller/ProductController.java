package com.bondarchuk.management.controller;

import com.bondarchuk.management.entities.Product;
import com.bondarchuk.management.exception.ProductAlreadyExistException;
import com.bondarchuk.management.exception.ProductNotExistsException;
import com.bondarchuk.management.service.ProductService;
import com.bondarchuk.management.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT') or hasRole('ROLE_USER')")
@RequestMapping(path = "/product")
public class ProductController {

    private ProductService products;
    private VendorService vendors;

    public ProductController() {
    }

    @Autowired
    public void setProducts(ProductService products) {
        this.products = products;
    }

    @Autowired
    public void setVendors(VendorService vendors) {
        this.vendors = vendors;
    }

    @GetMapping(path = "/productList")
    public String getProductList(Model model) {
        model.addAttribute("products", products.getList());
        return "products-list";
    }

    @GetMapping(path = "/{id}/details")
    public String showProductDetails(@PathVariable("id") String id, Model model){
        Product product = products.findProduct(id);
        model.addAttribute("product", product);
        return "product-details";
    }

    @GetMapping(path = "/addProduct")
    public String getAddProductView(Model model){
        model.addAttribute("vendors", vendors.getList());
        return "product-create";
    }

    @PostMapping(path = "/addProduct")
    public String postProduct(@Valid @ModelAttribute(name = "product") Product product,
                              @RequestParam("vendorId") String vendorId,
                              BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("vendors", vendors.getList());
            return "product-create";
        }

        try {
            products.create(product, vendorId);
        } catch (ProductAlreadyExistException ex) {
            model.addAttribute("error", ex.getMessage());
            return "error";
        }

        model.addAttribute("product", product);
        return "product-details";
    }

    @GetMapping(path = "/findProduct")
    public String findProductView() {
        return "product-find";
    }

    @GetMapping(path = "/find")
    public String findProduct(@RequestParam("name") String name, Model model) {
        Product product = null;

        try {
            product = products.findProductName(name);
        } catch (ProductNotExistsException ex) {
            model.addAttribute("error", ex.getMessage());
            return "product-find";
        }

        model.addAttribute("product", product);
        return "product-details";
    }

    @GetMapping(path = "/{id}/updatePage")
    public String updateProductView(@PathVariable("id") String id, Model model) {
        Product product = products.findProduct(id);
        model.addAttribute("product", product);
        model.addAttribute("vendors", vendors.getList());
        return "product-update";
    }

    @PostMapping(path = "/{id}/update")
    public String showProductUpdateView(@Valid @ModelAttribute(name = "product") Product product,
                                        @PathVariable("id") String id,  @RequestParam("vendorId") String vendorId,
                                        BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("product", products.findProduct(id));
            model.addAttribute("vendors", vendors.getList());
            return "product-update";
        }

        try {
            products.update(product, vendorId);
        } catch (ProductAlreadyExistException ex) {
            model.addAttribute("error", ex.getMessage());
            return "error";
        }

        model.addAttribute("product", product);
        return "product-details";
    }

    @GetMapping(path = "/{id}/delete")
    public String deleteProduct(@PathVariable("id") String id) {
        products.delete(id);
        return "index";
    }

    @ModelAttribute
    public Product defaultProduct() {
        return new Product();
    }
}
