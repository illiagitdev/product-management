package com.bondarchuk.management.controller;

import com.bondarchuk.management.entities.Vendor;
import com.bondarchuk.management.exception.VendorAlreadyExistsException;
import com.bondarchuk.management.exception.VendorNotExistsException;
import com.bondarchuk.management.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_ROOT') or hasRole('ROLE_USER')")
@RequestMapping(path = "/vendor")
public class VendorController {
    private VendorService vendors;

    public VendorController() {
    }

    @Autowired
    public void setVendors(VendorService vendors) {
        this.vendors = vendors;
    }

    @GetMapping(path = "addVendor")
    public String getAddVendorView() {
        return "vendor-create";
    }

    @PostMapping(path = "addVendor")
    public String createVendor(@ModelAttribute("vendor") Vendor vendor, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "vendor-create";
        }

        try {
            vendors.create(vendor);
        } catch (VendorAlreadyExistsException ex) {
            model.addAttribute("error", ex.getMessage());
            return "error";
        }

        model.addAttribute("vendor", vendor);
        return "vendor-details";
    }

    @GetMapping(path = "/{id}/updatePage")
    public String updateVendorView(@PathVariable("id") String id, Model model) {
        model.addAttribute("vendor", vendors.findVendor(id));
        return "vendor-update";
    }

    @PostMapping(path = "/{id}/update")
    public String updateVendor(@PathVariable("id") String id, @Valid @ModelAttribute("vendor") Vendor vendor,
                               BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("vendor", vendors.findVendor(id));
            return "vendor-update";
        }

         try {
             vendors.update(vendor);
         } catch (VendorAlreadyExistsException ex) {
             model.addAttribute("error", ex.getMessage());
             return "error";
         }

        model.addAttribute("vendor", vendor);
        return "vendor-details";
    }

    @GetMapping(path = "/{id}/delete")
    public String deleteVendor(@PathVariable("id") String id) {
        vendors.delete(id);
        return "index";
    }

    @GetMapping(path = "/vendorList")
    public String showVendorList(Model model) {
        model.addAttribute("vendors", vendors.getList());
        return "vendor-list";
    }

    @GetMapping(path = "/{id}/details")
    public String vendorDetails(@PathVariable("id") String id, Model model) {
        model.addAttribute("vendor", vendors.findVendor(id));
        return "vendor-details";
    }

    @GetMapping(path = "/findVendor")
    public String findVendorView() {
        return "vendor-find";
    }

    @GetMapping(path = "/find")
    public String findVendor(@RequestParam("name") String name, Model model) {
        Vendor vendor = null;

        try {
            vendor = vendors.findVendorName(name);
        } catch (VendorNotExistsException ex) {
            model.addAttribute("error", ex.getMessage());
            return "vendor-find";
        }

        model.addAttribute("vendor", vendor);
        return "vendor-details";

    }

    @ModelAttribute
    public Vendor defaultVendor() {
        return new Vendor();
    }
}
