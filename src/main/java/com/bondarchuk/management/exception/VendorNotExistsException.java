package com.bondarchuk.management.exception;

public class VendorNotExistsException extends RuntimeException {
    public VendorNotExistsException(String message) {
        super(message);
    }
}
